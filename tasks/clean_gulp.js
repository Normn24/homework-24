var gulp = require('gulp');
var clean = require('gulp-clean');

module.exports = function clean_gulp() {
  return gulp.src('build/', { read: false })
    .pipe(clean());
}