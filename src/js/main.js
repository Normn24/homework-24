window.addEventListener('scroll', function () {
  const navMenu = document.querySelector('.header__nav');
  const burgerMenu = document.querySelector(".menu__btn");
  const menuItem = document.querySelectorAll(".li__item")
  const viewHeight = 0;


  if (window.scrollY > viewHeight) {
    navMenu.classList.add('sticky');
    burgerMenu.classList.add("is-pinned");
    if (screen.width < 1200) {
      menuItem.forEach(element => {
        element.classList.add("is-pinned");
      })
    }
  } else {
    navMenu.classList.remove('sticky');
    burgerMenu.classList.remove("is-pinned");
    menuItem.forEach(element => {
      element.classList.remove("is-pinned");
    })
  }
});

const serviceItem = document.querySelectorAll(".service__list li")
if (screen.width < 1200) {
  Array.from(serviceItem).forEach((element, index) => {
    if (index == 2) {
      element.parentNode.removeChild(element);
    }
  });
}
if (screen.width < 768) {
  Array.from(serviceItem).forEach((element, index) => {
    if (index == 0) {
      element.parentNode.removeChild(element);
    }
  });
}
