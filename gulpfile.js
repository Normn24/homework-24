const gulp = require('gulp');
const requireDir = require('require-dir');
const tasks = requireDir('./tasks');

exports.clean_gulp = tasks.clean_gulp;
exports.style = tasks.style;
exports.build_js = tasks.build_js;
exports.dev_js = tasks.dev_js;
exports.html = tasks.html;
exports.js_minify = tasks.js_minify;
exports.rastr = tasks.rastr;
exports.bs_html = tasks.bs_html;
exports.watch = tasks.watch;

exports.default = exports.clean_gulp;

exports.default = gulp.parallel(
  exports.html,
  exports.style,
  exports.rastr,
  exports.build_js,
  exports.dev_js,
  exports.js_minify,
  exports.bs_html,
  exports.watch,
)
